package tetris.gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
 
public class ButtonExit extends JPanel implements ActionListener{
    public static final int HEIGHT = 100;
 	public static final int WIDTH = 300;
 	private JButton exitButton;
 	private Tetris TetrisObj;

	public static void loadScene(JFrame frame, JPanel panel){
        frame.setContentPane(panel);
        frame.invalidate();
        frame.validate();
        panel.requestFocus();
    }

    public ButtonExit(Tetris obj) {
 		exitButton = new JButton("Wyjście");
 	 	TetrisObj = obj;
 	 	exitButton.addActionListener(this);
 	 	setLayout(new FlowLayout());
 	 	setPreferredSize(new Dimension(HEIGHT, WIDTH));
 	 	add(exitButton);
 	}
 
 	public void actionPerformed(ActionEvent e) {
 		Object source = e.getSource();
		if(source == exitButton)
		{
			System.exit(0);
		}
 	}
}
