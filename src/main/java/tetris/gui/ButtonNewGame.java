package tetris.gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ButtonNewGame extends JPanel implements ActionListener{

    public static final int HEIGHT = 100;
    public static final int WIDTH = 300;
    private boolean isPlaying = false;
    private JButton playButton;
    private Tetris TetrisObj;

    public static void loadScene(JFrame frame, JPanel panel){
        frame.setContentPane(panel);
        frame.invalidate();
        frame.validate();
        panel.requestFocus();
    }

    public ButtonNewGame(Tetris obj) {
 		playButton = new JButton("Start");
 	 	TetrisObj = obj;
 	 	playButton.addActionListener(this);
 	 	setLayout(new FlowLayout());
 	 	setPreferredSize(new Dimension(HEIGHT, WIDTH));
 	 	add(playButton);
 	}

    public void actionPerformed(ActionEvent e) {
 		Object source = e.getSource();
		if(source == playButton & !isPlaying)
		{
			new Thread(new Runnable() {
			    public void run() {
			    	TetrisObj.gameLoop();
			    }
			}).start();
			isPlaying = true;
		}
 	}

}
