package tetris.gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
 
 public class button{
   private ButtonNewGame playButtonObj;
 	private ButtonExit exitButtonObj;

   public static void loadScene(JFrame frame, JPanel panel){
      frame.setContentPane(panel);
      frame.invalidate();
      frame.validate();
      panel.requestFocus();
   }

    public button(String text, Tetris obj, JPanel panel){
 	    if(text == "Nowa gra")
 		  {
 			    playButtonObj = new ButtonNewGame(obj);
 			    panel.add(playButtonObj);
 		  }
      if(text == "Wyjscie")
 		  {
 			  exitButtonObj = new ButtonExit(obj);
 			  panel.add(exitButtonObj);
 		  }
    }
     
 }
