package tetris.gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;
import javax.swing.JPanel;

import tetris.input.KeyboardInput;
import tetris.model.BoardCell;
import tetris.model.Game;
import tetris.model.PieceType;
import java.awt.EventQueue;

public class Tetris extends Canvas {

    private Game game = new Game();
    private final BufferStrategy strategy;

    private final int BOARD_CORNER_X = 300;
    private final int BOARD_CORNER_Y = 50;

    public final KeyboardInput keyboard = new KeyboardInput();
    public long lastIteration = System.currentTimeMillis();

    private static final int PIECE_WIDTH = 20;

    
    public Tetris() {
        JFrame container = new JFrame("Programowanie_w_Jezyku_Java_Projekt");
        JPanel panel = (JPanel) container.getContentPane();
        panel.setPreferredSize(new Dimension(800, 600));
        panel.setLayout(null);
        centerWindow(container);
        setBounds(0, 0, 800, 600);
        panel.add(this);
        setIgnoreRepaint(true);

        container.pack();
        container.setResizable(false);
        container.setVisible(true);

        container.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        addKeyListener(keyboard);
        requestFocus();

        createBufferStrategy(2);
        strategy = getBufferStrategy();
    }
    
    Runnable gameLoop() {
        while (true) {
        	
            if (keyboard.newGame()) {
                game = new Game();
                game.startGame();
            }
            if (game.isPlaying()) {

                if (!game.isPaused()) {
                    tetrisLoop();
                }
                if (keyboard.pauseGame()) {
                    game.pauseGame();
                }
            }
            try {
                Thread.sleep(20);
            } catch (Exception e) { }
            draw();
        }
    }
    
    void tetrisLoop() {
        if (game.isDropping()) {
            game.moveDown();
        } else if (System.currentTimeMillis() - lastIteration >= game.getIterationDelay()) {
            game.moveDown();
            lastIteration = System.currentTimeMillis();
        }
        if (keyboard.rotate()) {
            game.rotate();
        } else if (keyboard.left()) {
            game.moveLeft();
        } else if (keyboard.right()) {
            game.moveRight();
        } else if (keyboard.drop()) {
            game.drop();
        }
    }
    public void draw() {
            Graphics2D g = getGameGraphics();
            drawEmptyBoard(g);
            drawHelpBox(g);
            drawPiecePreviewBox(g);

            if (game.isPlaying()) {
                drawCells(g);
                drawPiecePreview(g, game.getNextPiece().getType());

                if (game.isPaused()) {
                    drawGamePaused(g);
                }
            }

            if (game.isGameOver()) {
                drawCells(g);
                drawGameOver(g);
            }

            drawStatus(g);
            drawPlayTetris(g);

            g.dispose();
            strategy.show();
    }
    
    private Graphics2D getGameGraphics() {
        return (Graphics2D) strategy.getDrawGraphics();
    }
    private void drawCells(Graphics2D g) {
        BoardCell[][] cells = game.getBoardCells();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 20; j++) {
                drawBlock(g, BOARD_CORNER_X + i * 20, BOARD_CORNER_Y + (19 - j) * 20, getBoardCellColor(cells[i][j]));
            }
        }
    }
    public static void centerWindow(JFrame f){
        Dimension windowSize = f.getPreferredSize();
        Dimension pos = Toolkit.getDefaultToolkit().getScreenSize();

        int dx = (pos.width / 2) - (windowSize.width / 2);
        int dy = (pos.height / 2) - (windowSize.height / 2);
        f.setLocation(dx, dy);

    }
    
    private void drawEmptyBoard(Graphics2D g) {
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, 800, 600);
        g.setColor(Color.WHITE);
        g.drawRect(BOARD_CORNER_X - 1, BOARD_CORNER_Y - 1, 10 * PIECE_WIDTH + 2, 20 * PIECE_WIDTH + 2);
    }

    private void drawStatus(Graphics2D g) {
        g.setFont(new Font("Dialog", Font.PLAIN, 16));
        g.setColor(Color.WHITE);
        g.drawString(getLevel(), 10, 20);
        g.drawString(getLines(), 10, 40);
        g.drawString(getScore(), 510, 60);
    }

    private void drawGameOver(Graphics2D g) {
        Font font = new Font("Dialog", Font.PLAIN, 16);
        g.setFont(font);
        g.setColor(Color.RED);
        g.drawString("GAME OVER", 345, 30);
    }

    private void drawGamePaused(Graphics2D g) {
        Font font = new Font("Dialog", Font.PLAIN, 16);
        g.setFont(font);
        g.setColor(Color.GREEN);
        g.drawString("GAME PAUSED", 345, 30);
    }

    private void drawPlayTetris(Graphics2D g) {
        Font font = new Font("Dialog", Font.PLAIN, 16);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString("Tetris", 370, 500);
    }

    private String getLevel() {
        return String.format("Poziom: %1s", game.getLevel());
    }

    private String getLines() {
        return String.format("Rzędy: %1s", game.getLines());
    }

    private String getScore() {
        return String.format("Wynik     %1s", game.getTotalScore());
    }

    private void drawPiecePreviewBox(Graphics2D g) {
        g.setFont(new Font("Dialog", Font.PLAIN, 16));
        g.setColor(Color.WHITE);
        g.drawString("Nastepny Blok:", 550, 420);
    }

    private void drawHelpBox(Graphics2D g) {
        g.setFont(new Font("Dialog", Font.PLAIN, 16));
        g.setColor(Color.WHITE);
        g.drawString("", 10, 490);
        g.drawString("F1: Przerwa", 10, 510);
        g.drawString("F2: Nowa gra", 10, 530);
        g.drawString("W	: Rotacja", 10, 550);
        g.drawString("A,D: Ruch", 10, 570);
        g.drawString("SPACE: Przyśpieszenie", 10, 590);
    }

    private void drawPiecePreview(Graphics2D g, PieceType type) {
        for (Point p : type.getPoints()) {
            drawBlock(g, 585 + p.x * PIECE_WIDTH, 380 + (3 - p.y) * 20, getPieceColor(type));
        }
    }

    private Color getBoardCellColor(BoardCell boardCell) {
        if (boardCell.isEmpty()) {
            return Color.BLACK;
        }
        return getPieceColor(boardCell.getPieceType());
    }

    private Color getPieceColor(PieceType pieceType) {
        switch (pieceType) {
            case I:
                return Color.RED;
            case J:
                return Color.ORANGE;
            case L:
                return Color.CYAN;
            case O:
                return Color.BLUE;
            case S:
                return Color.GREEN;
            default:
                return Color.MAGENTA;
        }
    }
    
    private void drawBlock(Graphics g, int x, int y, Color color) {
        g.setColor(color);
        g.fillRect(x, y, PIECE_WIDTH, PIECE_WIDTH);
        g.drawRect(x, y, PIECE_WIDTH, PIECE_WIDTH);
    }

    public static void main(String[] args) {
        Tetris gameTet = new Tetris();
    	ActionFrame menuTet = new ActionFrame(gameTet); 
        
    }
}
