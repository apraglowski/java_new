package tetris.gui;
import javax.swing.*;
 
 public class ActionFrame extends JFrame {
 
 	public ActionFrame(Tetris obj) {
 		super("Programowanie w Języku Java");
 		JFrame  frame = new JFrame ("Frame");
 		JPanel panel = new JPanel();
		new button("Nowa gra", obj, panel);
 		new button("Wyjscie", obj, panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 		frame.add(panel);
 		frame.pack();
        frame.setVisible(true);
 	}
 }
